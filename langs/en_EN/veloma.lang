# Copyright (C) 2022 Mikael Carlavan <contact@mika-carl.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# Générique
#

# Module label 'ModuleVelomaName'
ModuleVelomaName = Véloma
# Module description 'ModuleVelomaDesc'
ModuleVelomaDesc = Cycle-sharing system

#
# Page d'administration
#
VelomaSetup = Module setup
Settings = Settings
VelomaSetupPage = Setup page
VelomaOptions = Options
Description = Description
Value = Value
Action = Action
VelomaCurrentCredit = Current credit
VelomaRentLimit = Rental limit
DescVELOMA_MAP_LATITUDE = Map center (latitude)
DescVELOMA_MAP_LONGITUDE = Map center(longitude)
DescVELOMA_MAP_ZOOM = Map initial zoom
DescVELOMA_USE_CREDIT = Using the credit systemUtilisation du système de crédit
DescVELOMA_RENT_COST = Number of credits consumed over a period of time
DescVELOMA_RENT_DURATION = Number of minute for a period of time
DescVELOMA_FREE_DURATION = Number of minutes before using credit
DescVELOMA_ALLOW_UNREGISTERED_USERS = Unregistered users allowed
DescVELOMA_INITIAL_LIMIT = Initial rental limit value
DescVELOMA_INITIAL_CREDIT = Initial credit value
#
# Page À propos
#
About = À propos
VelomaAbout = About
VelomaAboutPage = About page
VelomaAboutDescLong = This module has been developped by <a href="http://www.mikael-carlavan.fr">Mikael Carlavan</a> for <a href="https://veloma.org">Véloma</a>.

# ChangeLog
ChangeLog = ChangeLog
ChangeLogVersion = Version
ChangeLogDate = Date
ChangeLogUpdates = Updates
VelomaFirstVersion = First version

#
VelomaBikes = Cycles
VelomaHistory = History
VelomaBike = Cycle
VelomaStand = Stand
VelomaUser = User
VelomaAction = Action
VelomaParameters = Parameters
VelomaDateStart = Start date
VelomaDateEnd = End date
VelomaDeleteBooking = Deleter
VelomaConfirmDeleteBooking = Are you sure you want to delete this booking?
# Commandes
VelomaHelpCommand = HELP
VelomaCreditCommand = CREDIT
VelomaFreeCommand = FREE
VelomaRentCommand = RENT
VelomaReturnCommand = RETURN
VelomaForceRentCommand = FORCERENT
VelomaForceReturnCommand = FORCERETURN
VelomaWhereCommand = WHERE
VelomaWhoCommand = WHO
VelomaInfoCommand = INFO
VelomaNoteCommand = NOTE
VelomaTagCommand = TAG
VelomaDelNoteCommand = DELNOTE
VelomaUnTagCommand = UNTAG
VelomaAddCommand = ADD
VelomaListCommand = LIST
VelomaRevertCommand = REVERT
VelomaLastCommand = LAST
VelomaUnknownCommand = Unknown command.
#
VelomaListCommands = Help :
VelomaHelpCommandDetails = %s : Help service
VelomaFreeCommandDetails = %s : Free cycles list
VelomaRentCommandDetails = %s X : Rent the cycle X
VelomaReturnCommandDetails = %s X Y : Return the cycle X to the station Y
VelomaWhereCommandDetails = %s X : Searching the cycle X
VelomaWhoCommandDetails = %s X : User of cycle X
VelomaInfoCommandDetails = %s X : Information about stand X
VelomaListCommandDetails = %s X : Cycles list of the stand X
VelomaAddCommandDetails = %s FIRSTNAME LASTNAME EMAIL PHONE CREDIT : Register the user FIRSTNAME NAME with EMAIL and optionnaly set its initial CREDIT.

#
VelomaNotRegistered = You're account hasn't been found. Please contact us for registering.
VelomaNoFreeBikes = No free cycle.
VelomaBikeNotFound = The cycle hasn't been found.
VelomaFreeBikes = Free cycles : %s.
VelomaBikeIsNotFree = The cycle is not available.
VelomaBikeRented = Renting confirmed. The cycle code is %s.
VelomaBikeBooked = Booking of cycle %s confirmed from %s to %s.
VelomaBikeBookCanceledByAdmin = An admin has canceled your booking of cycle %s from %s to %s.
VelomaEndBeforeStart = End date must be greater than start date.
VelomaBookCanceled = The booking has been canceled.
VelomaBookNotFound = The booking has not been found.
VelomaCancelBookDesc = Are you sure you want to cancel this booking?
VelomaCancelBook = Cancel booking
VelomaUserCreditIsInsufficient = Your credit balance is insufficient for the rental of this cycle.
VelomaUserTooManyBikesRented = You have exceeded the authorized number of rentals.
VelomaBikeReturned = The return of the cycle is recorded. The code was %s, please set the new code: %s.
VelomaBikeReturnedWithCredit = The return of the cycle is recorded. The code was %s, please set the new code: %s. Your new credit balance is %s.
VelomaBikeIsNotRentedByYou = You're renting this cycle.
VelomaBikeIsNotRented = The cycle is not rented.
VelomaBikeCommandNotAllowed = You're not allowed to process this command.
VelomaBikeNotRented = This cycle is not rented.
VelomaBikeIsRented = The cycle is currently rented.
VelomaBikeUserNotFound = The user of this cycle has not been found.
VelomaBikeStandNotFound = The cycle's stand has not been found.
VelomaStandNotFound = The stand has not been found.
VelomaBikeIsRentedBy = The cycle is rented by %s.
VelomaBikeIsLocatedAt = The cycle is located at the stand %s, GPS coordinates are %s,%s.
VelomaStandInfo = Information about the stand %s, %s,  located at the GPS coordinates %s, %s.
VelomaBikeNoteIsEmpty = The note is empty.
VelomaBikeNoteAdded = The note has been added to the cycle.
VelomaBiketagIsEmpty = The damage is empty.
VelomaBikeTagAdded = The damage has been added to the cycle.La dégradation a été ajoutée au vélo.
VelomaBikesInStand = Cycles of the station: %s.
VelomaNoBikesInStand = No cycle in the station.
VelomaBikeRentCanceled = The renting has been canceled.
VelomaUserAdded = The user has been added.
VelomaUserAlreadyExist = A user with this email already exists.
VelomaBikeLastNoteDeleted = Your last note has been deleted.
VelomaBikeNoNoteFound = No notes found.
VelomaStandLAstTagDeleted = The last damage has been deleted.
VelomaStandNoTagFound = No damage found.
VelomaBikeWasRentedBy = The last renter of the cycle is %s.
VelomaUserCredit = Your credit is %s units
VelomaErrorInCreatingUser = An error happened while creating the user. Please check the parameters used.

#
VelomaOpenMenu = Open the menu
VelomaCloseMenu = Close the menu
VelomaSignIn = Connection
VelomaSignUp = Create an account
VelomaMyRentals = My rentals
VelomaMyBookings = My bookings
VelomaRentABike = Rent a cycle
VelomaBookABike = Book a cycle
VelomaMetaDescription = Cycles rentals
VelomaMetaKeywords = Cycles rentals
VelomaApplicationTitle = Véloma
VelomaWelcomeTitle = Welcome to our sharing system of
VelomaRentalsTitle = My rentals of
VelomaBookings = Bookings
VelomaDatesAreEmpty = The start date and end date must be filled.
VelomaBookingsTitle = My bookings of 
VelomaWelcomeBikes = cycles
VelomaRentTitle = Rent a 
VelomaBookTitle = Book a
VelomaRentBike = cycle
VelomaBookBike = cycle
VelomaBookStartDate = Start date
VelomaBookEndDate = End date
VelomaBookSearch = Search
VelomaRentBikes = cycles
VelomaBookBikes = cycles
VelomaWelcomeDetails = You can rent or book a bike by clicking on the below buttons.
VelomaRentDetails = Click on the below map to select a cycle.
VelomaBookDetails = Click on the map below to select a cycle. You can also modify the start and end booking dates.
VelomaRentalsDetails = Find here all your current rentals.
VelomaBookingsDetails = Find here all your current bookings.
VelomaWelcomeRentBike = Rent a cycle
VelomaWelcomeBookBike = Book a cycle
VelomaAlreadyHaveAnAccount = Do you already have an account here?
VelomaAllRightsReserved = All rights reserved.
VelomaDismiss = Close
VelomaLogOut = Log out
VelomaCancel = Cancel
VelomaLogIn = Log in
VelomaMyAccount = My account
VelomaSignInEmail = Email
VelomaSignInPassword = Password
VelomaPasswordForgotten = Forgotten password ?
VelomaValidate = Validate
VelomaPasswordRequestDesc = Fill in the phone number used.
VelomaPasswordRequest = Password forgotten
VelomaLogin = Login
VelomaLoginOrEmail = Login or email
VelomaEmail = Email address
VelomaFirstName = Firstname
VelomaLastName = Lastname
VelomaPhone = Phone number
VelomaPhoneOrEmail = Phone number or email
VelomaPassword = Password
#
VelomaBikeCode = Cycle code
VelomaEmailFieldIsMissing = Email address is missing.
VelomaPhoneFieldIsMissing = The phone number is missing.
VelomaLastNameFieldIsMissing = The lastname is missing.
VelomaFirstNameFieldIsMissing = The firstname is missing.
VelomaLoginFieldIsMissing = The login is missing.
VelomaPasswordFieldIsMissing = The password is missing.
VelomaConfirmationCodeSent = The confirmation code has been sent.
VelomaConfirmationCodeSms = Your confirmation code is %s.
VelomaAccountUpdated = Your account has been updated.
VelomaErrorWhileUpdatingUser = Error while updating your account.
VelomaErrorWhileSensingConfirmationCode = Error while sending the confirmation code.
VelomaErrorWhileGeneratingConfirmationCode = Error while generating the confirmation code.
VelomaPasswordSent = The password has been sent.
VelomaPasswordSms = Here is your new password %s.
VelomaConfirmationCodeDoesNotMatch = The confirmation code doesn't match.
VelomaUserNotFound = The user has not been found.
VelomaErrorWhileSendingNewPassword = Error while sending your new password.
VelomaErrorWhileCreatingUser = Error while creating your account.
VelomaErrorWhileSavingFile = Error while saving your file.
VelomaErrorWhileCreatingUserDirectory = Error while creating your user directory.
VelomaAccessNotAllowed = You're not allowed to do this.
VelomaEmailAddressAlreadyUsed = This email address is already used.
VelomaPhoneAlreadyUsed = This phone number is already used.
VelomaLoginAlreadyUsed = This login is already used.
VelomaConfirmRentDetails = Thanks for filling in the form for confirming your rental.
VelomaConfirmBookDetails = Thanks for filling in the following form for confirming your booking.
VelomaIncorrectEmailAddress = This email address is not correct. 
VelomaSubjectRequestPassword = Reset password
VelomaSubjectResetPassword = Reset password
VelomaRequestPasswordReceived = Hello, <br /><br />We received a password reset request for your account.
VelomaResetPassword = Your password has been modified.
VelomaNewPassword = You new password is <b>%s</b>.
VelomaConfirmationCode = Please go back into the application and fill in this confirmation code <b>%s</b>. Your new password will be sent afterwards.
VelomaForgetIfNothing = If you haven't requested to reset your password, you can ignore your message. Your login and password are still safe.<br />Have a good day.
VelomaPasswordValidation = Forgotten password
VelomaValidationCode = Validation code
VelomaRentStartedAt = Rental start date
VelomaNoRentals = No rental found.
VelomaBookingEvent = Booking of cycle %s
VelomaActionComm = Agenda
VelomaHaveANice = Have a nice
VelomaTrip = trip !
VelomaNoBookings = No booking found.
VelomaReturnBike = Return the cycle
VelomaReturnBikeDesc = Please fill in the stand where the cycle has been returned.
VelomaNotLoggedIn = Please login for accessing this information.
VelomaWelcomeNewUser = You've been correctly registered. Here is your new password: %s.
VelomaPasswordValidationDesc = A confirmation code has been sent by SMS. Please fill it in below.
VelomaRentBikeMarker = <div class="text-center"><div class="flex justify-center items-center"><div class="w-1/2"><img class="w-auto" src="%s" /></div><div class="w-1/2 pl-1 text-left items-center"><div class="whitespace-nowrap">Cycle  <b>%s</b></div><div class="whitespace-nowrap">Stand <b>%s</b></div></div></div><div class="mt-1"><a href="%s" class="flex w-full items-center justify-center rounded-md border border-transparent px-4 py-1 whitespace-nowrap text-sm font-medium bg-gray-100 hover:bg-gray-200">Rent this cycle</a></div></div>
VelomaBookBikeMarker = <div class="text-center"><div class="flex justify-center items-center"><div class="w-1/2"><img class="w-auto" src="%s" /></div><div class="w-1/2 pl-1 text-left items-center"><div class="whitespace-nowrap">Cycle <b>%s</b></div><div class="whitespace-nowrap">Stand <b>%s</b></div></div></div><div class="mt-1"><a href="%s" class="flex w-full items-center justify-center rounded-md border border-transparent px-4 py-1 whitespace-nowrap text-sm font-medium bg-gray-100 hover:bg-gray-200">Book this cycle</a></div></div>
VelomaStandMarker = <div class="text-center"><div class="flex justify-center items-center"><div class="w-1/2"><img class="w-auto" src="%s" /></div><div class="w-1/2 whitespace-nowrap pl-1 text-left items-center">Stand <b>%s</b></div></div></div>
