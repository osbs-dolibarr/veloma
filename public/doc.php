<?php
/* Copyright (C) 2009 Laurent Destailleur  <eldy@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *     	\file       htdocs/veloma/public/index.php
 *		\ingroup    core
 */

define('NOREQUIREMENU', 1);
define('NOLOGIN', 1);

$res=@include("../../main.inc.php");                   // For root directory
if (! $res) $res=@include("../../../main.inc.php");    // For "custom" directory

require_once DOL_DOCUMENT_ROOT.'/core/lib/functions.lib.php';
require_once DOL_DOCUMENT_ROOT.'/core/lib/security2.lib.php';
require_once DOL_DOCUMENT_ROOT.'/core/class/CMailFile.class.php';

dol_include_once("/veloma/class/site.class.php");
dol_include_once("/veloma/class/veloma.class.php");
dol_include_once("/veloma/class/veloma.history.class.php");

$action = GETPOST('action', 'alpha');

$langs->loadLangs(array('main', 'errors'));
$langs->load('veloma@veloma');
$langs->load("other");

$veloma = new Veloma($db);

$site = new Site($db);
$site->start($user);

$confirmationModalOpened = false;

if ($action == 'login')
{
	$site->login($user);
}

if ($action == 'register')
{
	$site->register($user);
}

if ($action == 'account')
{
    $site->account($user);
}

if ($action == 'passrequest')
{
    $result = $site->passwordrequest();
	if ($result > 0) {
        $confirmationModalOpened = true;
	}
}

if ($action == 'passvalidation')
{
	$site->passwordvalidation();
}

?>

<?php include_once('tpl/layouts/header.tpl.php'); ?>
    <h1 class="text-4xl text-center font-bold tracking-tight text-gray-900 sm:text-5xl md:text-6xl lg:text-5xl xl:text-6xl">
        <span class="block xl:inline">Fonctionnement</span>

<!--        <span class="block text-green-600 xl:inline"><?php echo $langs->trans('VelomaWelcomeBikes'); ?></span > -->
    </h1>
<div class="px-4 sm:px-8 xl:pr-16">
<br>
Ce système de réservation de cycles est basé sur <a href="htps://www.dolibarr.org" class="text-green-600 xl:inline">Dolibarr</a>, un logiciel libre de gestion d'entreprise et d'association. Un module dédié a été créé en s'appuyant sur le fonctionnement d'<a href="https://github.com/cyklokoalicia/OpenSourceBikeShare" class="text-green-600 xl:inline">OpenSourceBikeShare</a>.<br>
Une fois enregistré dans le système il vous est possible d'emprunter ou de louer un vélo par l'interface web sur laquelle tu es ou par SMS en utilisant des mots-clés prédéfinis.<br>
Le code source des modules est accessible ici: <a class="text-green-600" href="https://framagit.org/osbs-dolibarr/">https://framagit.org/osbs-dolibarr/</a>.<br>
Un guide d'installation et d'utilisation est disponible <a href="https://framagit.org/Veloma/Charrette/-/tree/master/Informatique/OpenSourceBikeShare" class="text-green-600">ici</a>.<br>
<br>
Voici la liste de ces mots-clés:<br><br>

</div>

<div class="flex flex-col">
  <div class="overflow-x-auto sm:-mx-6 lg:-mx-8">
    <div class="py-2 inline-block min-w-full sm:px-6 lg:px-8">
      <div class="overflow-auto">
        <table class="min-w-full">
          <thead class="bg-white border-b">
            <tr>
              <th scope="col" class="text-sm font-medium text-gray-900 px-6 py-4">
	      <b><?php echo $langs->trans('VelomaDocTableKeyword');?></b> 
              </th>
              <th scope="col" class="text-sm font-medium text-gray-900 px-6 py-4">
		<b><?php echo $langs->trans('VelomaDocTableMeaning');?></b>
              </th>
              <th scope="col" class="text-sm font-medium text-gray-900 px-6 py-4">
                <b><?php echo $langs->trans('VelomaDocTableSyntax');?></b>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr class="bg-gray-100 border-b">
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaHelpCommand');?>		
	      </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaHelpCommandLegend');?>		
              </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaHelpCommandSyntax');?>		
              </td>
            </tr>
            <tr class="bg-white border-b">
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaCreditCommand');?>		
	      </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaCreditCommandLegend');?>		
              </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaCreditCommandSyntax');?>		
              </td>
            </tr>
            <tr class="bg-gray-100 border-b">
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaFreeCommand');?>		
	      </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaFreeCommandLegend');?>		
              </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaFreeCommandSyntax');?>		
              </td>
            </tr>
            <tr class="bg-white border-b">
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaRentCommand');?>		
	      </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaRentCommandLegend');?>		
              </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaRentCommandSyntax');?>		
              </td>
            </tr>
            <tr class="bg-gray-100 border-b">
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaReturnCommand');?>		
	      </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaReturnCommandLegend');?>		
              </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaReturnCommandSyntax');?>		
              </td>
            </tr>
            <tr class="bg-white border-b">
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaForceRentCommand');?>		
	      </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaForceRentCommandLegend');?>		
              </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaForceRentCommandSyntax');?>		
              </td>
            </tr>
            <tr class="bg-gray-100 border-b">
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaForceReturnCommand');?>		
	      </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaForceReturnCommandLegend');?>		
              </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaForceReturnCommandSyntax');?>		
              </td>
            </tr>
            <tr class="bg-white border-b">
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaWhereCommand');?>		
	      </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaWhereCommandLegend');?>		
              </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaWhereCommandSyntax');?>		
              </td>
            </tr>
            <tr class="bg-gray-100 border-b">
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaWhoCommand');?>		
	      </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaWhoCommandLegend');?>		
              </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaWhoCommandSyntax');?>		
              </td>
            </tr>
            <tr class="bg-white border-b">
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaInfoCommand');?>		
	      </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaInfoCommandLegend');?>		
              </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaInfoCommandSyntax');?>		
              </td>
            </tr>
            <tr class="bg-gray-100 border-b">
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaNoteCommand');?>		
	      </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaNoteCommandLegend');?>		
              </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaNoteCommandSyntax');?>		
              </td>
            </tr>
            <tr class="bg-gray-100 border-b">
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaDelNoteCommand');?>		
	      </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaDelNoteCommandLegend');?>		
              </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaDelNoteCommandSyntax');?>		
              </td>
            </tr>
            <tr class="bg-gray-100 border-b">
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaListCommand');?>		
	      </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaListCommandLegend');?>		
              </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaListCommandSyntax');?>		
              </td>
            </tr>
            <tr class="bg-gray-100 border-b">
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaAddCommand');?>		
	      </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaAddCommandLegend');?>		
              </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaAddCommandSyntax');?>		
              </td>
            </tr>
            <tr class="bg-gray-100 border-b">
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaRevertCommand');?>		
	      </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaRevertCommandLegend');?>		
              </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaRevertCommandSyntax');?>		
              </td>
            </tr>
            <tr class="bg-gray-100 border-b">
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaLastCommand');?>		
	      </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaLastCommandLegend');?>		
              </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
		<?php echo $langs->trans('VelomaLastCommandSyntax');?>		
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<br>
<div class="px-4 sm:px-8 xl:pr-16">
<!--
Pour plus d'informations merci de nous contacter:
<ul>
<li><?php echo  $conf->global->MAIN_INFO_SOCIETE_TEL;?></li>
<li><?php echo  $conf->global->MAIN_INFO_SOCIETE_MAIL;?></li>
</ul>
-->
</div>

<?php include_once('tpl/layouts/footer.tpl.php'); ?>
